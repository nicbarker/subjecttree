@SubjectManager = ->
  # Private variables and functions
  _data = {}
  _listView = null
  _totalCredits = 0
  _pinnedView = null

  updateCreditPoints = ->
    $('#credit-points-counter').html _totalCredits

  # The view for each subject bubble
  subjectView = Backbone.View.extend(
    className: 'subject-outer'
    template: _.template $('#subject-template').html()
    prereqLine: null
    events:
      'click': 'pin'
      'mouseover': 'preview'
      'mouseout': 'removePreview'
      'dblclick': 'select'

    initialize: ->
      @model.view = @

    # On double click, select the subject and calculate credit points
    select: ->
      if @model.get('selected')
        @$el.removeClass('selected')
        _totalCredits -= @model.get('credits')
        @model.set('selected', false)
      else
        @$el.addClass('selected')
        _totalCredits += @model.get('credits')
        @model.set('selected', true)
      updateCreditPoints()

    pin: ->
      if _pinnedView && _pinnedView != @
        _pinnedView.$el.removeClass('preview')
        _pinnedView.prereqLine.remove() if _pinnedView.prereqLine
        _pinnedView.$el.find('.tooltip').remove()
      _pinnedView = @
      # We don't want any more events firing after this one
      return false

    # On single click, highlight the subject and show dependencies and info
    preview: ->
      # Don't do anything if the view is already pinned
      return if _pinnedView == @
      @$el.addClass('preview')
      # If the subject has prerequisites, show them.
      # if @model.get('prerequisites')
      #   # Draw a line from this subject to it's prereqs
      #   _(@model.get('prerequisites')).each (model) ->
      #     if prereq = _listView.findSubjectByCode(model)
      #       prereq.view.$el.addClass('preview')
      #       $fromElement = @$el.find('.bubble')
      #       $toElement = prereq.view.$el.find('.bubble')
      #       x1 = $fromElement.offset().left + _listView.$outerEl.scrollLeft() + $fromElement.width() / 2
      #       y1 = $fromElement.offset().top + _listView.$outerEl.scrollTop() + $fromElement.height() / 2
      #       x2 = $toElement.offset().left + _listView.$outerEl.scrollLeft() + $toElement.width() / 2
      #       y2 = $toElement.offset().top + _listView.$outerEl.scrollTop() + $toElement.height() / 2
      #       @prereqLine = drawLine($('#canvas-inner'), x1, y1, x2, y2)
      #       _prereqLine = @prereqLine
      #   , @

      # Show the tooltip with information about the subject
      tooltip = _.template $('#tooltip-template').html()
      @$el.append(tooltip(@model.toJSON()))

    removePreview: ->
      # Don't remove the preview on mouse out if it's pinned
      return if _pinnedView == @
      @$el.removeClass('preview')
      @prereqLine.remove() if @prereqLine
      @$el.find('.tooltip').remove()

    render: ->
      @$el.html @template(@model.toJSON())
      this
  )

  model = Backbone.Model.extend(
    defaults:
      year: 1
      semester: 1
      code: "BIOL3023"
      title: "Biology of watching animals poop."
      description: "missing description"
      credits: 6
      selected: false
  )

  collection = Backbone.Collection.extend(
    model: model
  )

  # The container view for the whole canvas
  listView = Backbone.View.extend(
    el: '.canvas-inner'
    $outerEl: $('.canvas-outer')

    initialize: ->
      @collection = new collection(_data);
      @collection.bind 'add', @appendItem
      @render()

    events:
      'click': 'clickBackground'
          
    clickBackground: ->
      # When we click anywhere on the background, undo the current preview
      $('.preview').removeClass('preview')
      # Remove any pre requisites that are showing
      $('.prerequisite-line').remove()
      # Remove any visible tooltips
      $('.tooltip').remove()
      # Unpin any pinned subjects
      _pinnedView = null

    render: ->
      _(@collection.models).each (model) ->
        @appendItem model;
      , @

    appendItem: (model) ->
      view = new subjectView({model: model})
      @$el.find("#subject-row-#{model.get('year')}-#{model.get('semester')}").append(view.render().el)

    findSubjectByCode: (code) ->
      toReturn = null
      _.each @collection.models, (model) ->
        toReturn = model if model.get('code') == code
      return toReturn
  )

  # Public variables and functions

  initialize: (data) ->
    _data = data
    _listView = new listView()

$ ->
  Canvas.initCanvas 'junk'
  Canvas.placeObjects()
  # Center the viewport - the x value needs to be negative to move the screen left
  Canvas.setViewPort ($('#canvas-inner').width() / 2) - ($('#canvas-outer').width() / 2), 0

  # firstElement = $($('.bubble')[0])
  # secondElement = $($('.bubble')[3])
  # x1 = firstElement.position().left + firstElement.width() / 2
  # y1 = firstElement.offset().top + firstElement.height() / 2
  # x2 = secondElement.position().left + secondElement.width() / 2
  # y2 = secondElement.offset().top + secondElement.height() / 2

  # drawLine($('#canvas-inner'), x1, y1, x2, y2)

  # Fix an issue with firefox :active psuedoselector
  $('.clickable').on 'mousedown', ->
    $(this).addClass('active')
  $('.clickable').on 'mouseup', ->
    $(this).removeClass('active')
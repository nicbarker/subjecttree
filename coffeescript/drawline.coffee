# Draw a line from (x1,y1) to (x2,y2) and appends it to parentElement
@drawLine = (parentElement, x1, y1, x2, y2) ->
  if y1 < y2
    pom = y1
    y1 = y2
    y2 = pom
    pom = x1
    x1 = x2
    x2 = pom

  # Length of opposite and adjacent in right triangle
  a = Math.abs(x1 - x2) 
  b = Math.abs(y1 - y2)
  c = undefined
  sx = (x1 + x2) / 2
  sy = (y1 + y2) / 2
  # Length of hypotenuse in right triangle
  width = Math.sqrt(a * a + b * b)
  # Find angle of rotation in hypotenuse from start point
  x = sx - width / 2
  y = sy
  a = width / 2
  c = Math.abs(sx - x)
  b = Math.sqrt(Math.abs(x1 - x) * Math.abs(x1 - x) + Math.abs(y1 - y) * Math.abs(y1 - y))
  cosb = (b * b - a * a - c * c) / (2 * a * c)
  rad = Math.acos(cosb)
  deg = (rad * 180) / Math.PI

  # Append the element to the parent
  htmlns = "http://www.w3.org/1999/xhtml"
  div = document.createElementNS(htmlns, "div")
  div.setAttribute "style", "
    width: #{width}px;
    height: 0px;
    -moz-transform: rotate(#{deg}deg);
    -webkit-transform: rotate(#{deg}deg);
    position: absolute;
    top:#{y}px;
    left:#{x}px;"
  $(div).addClass('prerequisite-line visible')
  $(parentElement).append div
  return $(div)
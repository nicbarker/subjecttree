# Fake data until we connect it to a real backend - or maybe all the subject data should just be chukned to the client?

# This is a global object that manages "Canvas" that all the subjects etc sit on top of.
window.Canvas = (->
  # Jquery selectors and class names
  _canvasSelector = '#canvas-inner'
  _$canvasElement = null
  _canvasOuterSelector = '#canvas-outer'
  _$canvasOuterElement = null
  # Variables affecting subject bubbles
  _bubbleWidth = 120;
  _bubbleHorizontalMargin = 100;
  _bubbleHeight = 120;
  _bubbleVerticalMargin = 100;
  # Other variables
  _maxSubjects = null # The max number of subjects in any semester, defining total width of the canvas
  _dragObj = null # The javascript "draggable" object managing the viewport (js/draggable.js)
  _totalCredits = 0 # Keep track of the total selected credit points
  _subjectManager = null

  # Place the subject objects onto the canvas, in a centered alignment
  placeObjects: ->
    _subjectManager = new SubjectManager
    _subjectManager.initialize(DATA)

  initCanvas: (data) ->
    # Set up jquery element handlers so we don't have to select them again later
    _$canvasElement = $(_canvasSelector)
    _$canvasOuterElement = $(_canvasOuterSelector)
    # Count years and semesters of subjects, to work out width of canvas
    counter = {}
    for subject in DATA # Loop through subject data, and count which semester / year combo has the most subjects
      counter["#{subject.year}-#{subject.session[0]}"] = (counter["#{subject.year}-#{subject.session[0]}"] || 0) + 1

    # Get the number of subjects in most populous year / semester combo with a splat
    counter_splat = $.map counter, (value, index) ->
      value
    _maxSubjects = Math.max counter_splat...
    # After we've updated counters, set the boundaries
    _$canvasElement.width _maxSubjects * (_bubbleWidth + _bubbleHorizontalMargin)
    _$canvasElement.height 6 * (_bubbleHeight + _bubbleVerticalMargin)

    # Create the row divs based on the number of semesters
    row = 0
    _.each counter, (subjectNumber, semesterKey) ->
      # Create a new row with a year-semester key in the id so we can find it later
      $row_element = $("<div id=\"subject-row-#{semesterKey}\" class=\"subject-row\"></div>")
      _$canvasElement.append($row_element)
      row++

    # Make our inner canvas draggable
    rightEdge = _$canvasElement.width() - _$canvasOuterElement.width()
    bottomEdge = _$canvasElement.height() - _$canvasOuterElement.height()

    _dragObj = new dragObject('canvas-inner', null, new Position(0, 0), new Position(rightEdge, bottomEdge))

  setViewPort: (x,y) ->
    _dragObj.setPosition x,y if _dragObj
)()
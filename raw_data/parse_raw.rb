require 'json'

counter = 1
objects = []
file = File.new("usyd_science_handbook_2011_modified.txt", "r")
while (line = file.gets)
    line = line.gsub(/\n/, '')
    line = line.gsub(/\'/, '')
    next if line.length == 0

    if line.match(/code:\ [A-Z]{4}[0-9]{4}/)
      objects << {}
    end
    if line.match(/^.{1,20}:/)
      parts = line.partition(/:/)
      if parts.first.match(/code/)
        objects.last['year'] = parts.last.gsub(' ', '')[4]
        objects.last['code'] = parts.last.gsub(' ', '')
      elsif parts.last.match(/\[.+\]/) # It's an array element
        objects.last[parts.first] = parts.last.gsub(' ', '').gsub(/[\[|\]]/, '').split(/,/)
      else
        objects.last[parts.first] = parts.last
      end
    else
      objects.last['description'] = "#{objects.last['description']}#{line}"
    end
    counter += 1
end
file.close

File.write('../js/data.js', "DATA = #{JSON.dump(objects)}")